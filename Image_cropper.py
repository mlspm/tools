from PIL import Image
import PIL.ExifTags
import os
 
def crop_in_half(image_path, saved_location):
    """
    @param image_path: The path to the image to edit
    @param coords: A tuple of x/y coordinates (x1, y1, x2, y2)
    @param saved_location: Path to save the cropped image
    Transfers EXIF-data
    """
    image_obj = Image.open(image_path)
    width, height = image_obj.size   # Get dimensions
    new_width = width/2
    new_height = height
    x = 0
    y = 0
    right = new_width
    bottom = height

    exif_data = image_obj.info['exif']
    cropped_image = image_obj.crop((x,y,right,bottom))
    cropped_image.save(saved_location, exif = exif_data) 
    
    
if __name__ == '__main__':
    location = 'E:/TestCropImages' # Path to images
    img_names = [] # Empty list to hold img names
    # Fetch all jpg files in a directory
    for file in os.listdir(location):
        if file.endswith(".jpg"): 
                img_names.append(str(file))
    print('Found {} images'.format(len(img_names)))
    
    # Crops all images. Unless last parameter in crop_in_half function is changed 
    # it will overwrite the images in the path
    counter = 0
    for image in img_names:
        counter += 1
        image_path = location + '/' + image
        crop_in_half(image_path, image_path)
        print('Cropping image {} of {}'.format(counter, len(img_names)),'\r', end='', flush=True)
     