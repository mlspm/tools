import PIL.ExifTags
from PIL import Image
from PIL.ExifTags import TAGS, GPSTAGS

def fetch_metadata_gps(filename):
    img = Image.open(filename)
    exif_data = img._getexif()
    for k, value in exif_data.items():
        tags = TAGS.get(k, k)
        if tags == "GPSInfo":
            gps_data = {}
            for t in value:
                sub_decoded = GPSTAGS.get(t,t)
                gps_data[sub_decoded] = value[t]

    return gps_data

def _get_if_exist(data, key):
    if key in data:
        return data[key]
		
    return None

def extract_lat_lon(gps_info):
    # EXTRACT TUPLES OF COORDINATES FROM EXIF DATA
    gps_latitude = _get_if_exist(gps_info, "GPSLatitude")
    gps_latitude_ref = _get_if_exist(gps_info, 'GPSLatitudeRef')
    gps_longitude = _get_if_exist(gps_info, 'GPSLongitude')
    gps_longitude_ref = _get_if_exist(gps_info, 'GPSLongitudeRef')
    
    # CONVERTS TO A SEARCHABLE DMS, DEGREE MINUTES AND SECONDS
    # DIVIDE THE FIST ENTRY IN THE TUPLE WITH THE SECOND ENTRY TO 
    # GET CORRECT DECIMAL POINT.
    lat_deg = float(gps_latitude[0][0]) / float(gps_latitude[0][1])
    lat_min = float(gps_latitude[1][0]) / float(gps_latitude[1][1])
    lat_sec = float(gps_latitude[2][0]) / float(gps_latitude[2][1])
    lat = lat_deg,lat_min,lat_sec
    str_lat = str(lat_deg) + "d" + str(lat_min)+ "'" + str(lat_sec) + '"' + gps_latitude_ref

    lon_deg = float(gps_longitude[0][0]) / float(gps_longitude[0][1])
    lon_min = float(gps_longitude[1][0]) / float(gps_longitude[1][1])
    lon_sec = float(gps_longitude[2][0]) / float(gps_longitude[2][1])
    lon = lon_deg,lon_min,lon_sec
    str_lon = str(lon_deg) + "d" + str(lon_min)+ "'" + str(lon_sec) + '"' + gps_longitude_ref

    # RETURNS PURE FLOATS AND STRING VERSION. 
    # NOTE THAT STRING VERSIONS HAVE TO REPLACE
    # 'd' WITH DEGREE SYMBOL TO MAKE IT SEARCHABLE
    return lat, lon, str_lat, str_lon



if __name__ == "__main__":
    path = "E:/"
    filename = "Fy01_Fv112_hp01_f1_m05917.jpg"
    test = fetch_metadata_gps(path+filename)
    lat, lon, str_lat, str_lon = extract_lat_lon(test)
    
    print(lat)
    print(lon)
    print(str_lat + str_lon)